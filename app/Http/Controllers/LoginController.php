<?php


namespace App\Http\Controllers;


use App\Models\Admin;
use Illuminate\Http\Request;
use Session;

class LoginController
{
    public function view_login (  ) {
        return view( "view_login");
    }
    public function process_login ( Request $request) {
        try {
            $admin = Admin::where([
                'email' => $request->email,
                'password' => $request->password,
            ])->firstOrFail();

            Session::put('id', $admin->id);
            Session::put('name', $admin->name);

        }
        catch ( \Exception $e ) {
            return redirect()->back()->with('error','Fail');
        }

    }

    public function logout (  ) {
        Session::flush();

        return redirect()->route('view_login');
    }
}