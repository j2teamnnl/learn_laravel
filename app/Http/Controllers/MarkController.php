<?php

namespace App\Http\Controllers;

use App\Models\Mark;
use App\Models\MarksAdvanced;
use App\Models\School;
use App\Models\Student;
use App\Models\Subject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MarkController
{
    public function choose () {
        $schools  = School::get();
        $subjects = Subject::get();
        return view( "marks.choose", compact( 'schools', 'subjects' ) );
    }

    public function view ( Request $request ) {
        $schools  = School::get();
        $subjects = Subject::get();

        $school_id  = $request->school_id;
        $subject_id = $request->subject_id;


        $students = Student::query()
            ->where( 'school_id', $school_id )
            ->get();

        $marksWithStudents = Student::query()
            ->join( 'marks', 'marks.student_id', 'students.id' )
            ->where( 'subject_id', $subject_id )
            ->where( 'school_id', $school_id )
            ->get();

        DB::connection()->enableQueryLog();
        $max_times_by_type = Student::query()
            ->select( 'type', DB::raw( 'max(time) as max_time' ) )
            ->join( 'marks', 'marks.student_id', 'students.id' )
            ->where( 'subject_id', $subject_id )
            ->where( 'school_id', $school_id )
            ->groupBy( 'type' )
            ->get();
//        dd( DB::getQueryLog() );
        $max_times = [
            1,
            1
        ];
        foreach ( $max_times_by_type as $each ) {
            $max_times[ $each->type ] = $each->max_time + 1;
        }

        $marks = [];
        foreach ( $marksWithStudents as $student ) {
            $marks[ $student->id ][ $student->type ][ $student->time ] = $student->mark;
        }
        return $max_times_by_type;
        return view( "marks.view", compact(
            'schools',
            'subjects',
            'students',
            'school_id',
            'subject_id',
            'max_times',
            'marks',
        ) );
    }

    public function process ( Request $request ) {
        $marks      = $request->marks;
        $subject_id = $request->subject_id;
        foreach ( $marks as $student_id => $types ) {
            foreach ( $types as $type => $times ) {
                foreach ( $times as $time => $mark ) {
                    if ( $mark ) {
                        Mark::updateOrCreate( [
                            'student_id' => $student_id,
                            'subject_id' => $subject_id,
                            'type'       => $type,
                            'time'       => $time,
                        ], [
                            'mark' => $mark
                        ] );
                    }
                    if ( $mark == 0 ) {
                        Mark::where( [
                            'student_id' => $student_id,
                            'subject_id' => $subject_id,
                            'type'       => $type,
                            'time'       => $time,
                        ] )->delete();
                    }
                }
            }
        }
    }

    public function choose_advanced () {
        $schools  = School::get();
        $subjects = Subject::get();
        return view( "marks_advanced.choose", compact( 'schools', 'subjects' ) );
    }

    public function view_advanced ( Request $request ) {
        $schools  = School::get();
        $subjects = Subject::get();

        $school_id  = $request->school_id;
        $subject_id = $request->subject_id;


        $student_with_mark = Student::query()
            ->with( [
                'marks_with_info' => function ( $query ) use ( $subject_id ) {
                    $query->where( 'subject_id', $subject_id );
                }
            ] )
            ->where( 'school_id', $school_id )
            ->basicInfo()
            ->get();

        $marks = [];
        foreach ( $student_with_mark as $student ) {
            foreach ( $student->marks_with_info as $mark ) {
                $marks[ $student->id ][ $mark->type ][ $mark->time ] = $mark->mark;
            }
        }
//        DB::connection()->enableQueryLog();
        $max_times_each_type = MarksAdvanced::query()
            ->whereHas( 'student', function ( $query ) use ( $school_id ) {
                $query->where( 'school_id', $school_id );
            } )
            ->select( DB::raw( 'max(time) as max_time' ), 'type' )
            ->groupBy( 'type' )
            ->get();
        return $max_times_each_type;
//        dd( DB::getQueryLog() );
        return view( "marks_advanced.view", compact(
            'schools',
            'subjects',
            'school_id',
            'subject_id',
            'max_times_each_type',
            'student_with_mark',
            'marks',
        ) );
    }

}