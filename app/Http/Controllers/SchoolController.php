<?php

namespace App\Http\Controllers;

use App\Models\School;
use Illuminate\Http\Request;

class SchoolController
{
    public $table = 'schools';

    public function index ( Request $request ) {
        $search  = $request->search;
        $schools = School::withCount( 'students' )
            ->where( 'name', 'like', "%$request->search%" )
            ->paginate( 1 );


        return view( "$this->table.index", compact( 'schools', 'search' ) );
    }

    public function create () {
        return view( "$this->table.create" );
    }

    public function store ( Request $request ) {
        School::create( $request->all() );
    }

    public function show ( School $school ) {
        return $school;

        School::updateOrCreate( [
            'ma_sinh_vien' => $request->ma_sinh_vien,
            'ma_mon'       => $request->ma_mon,
        ], [
            'diem' => $request->diem,
        ] );
    }

    public function edit ( School $school ) {
        return view( "$this->table.edit", compact( 'school' ) );
    }

    public function update ( Request $request, School $school ) {
        $school->update( $request->all() );
    }

    public function destroy ( School $school ) {
        $school->delete();

        return redirect()->route( "$this->table.index" );
    }
}
