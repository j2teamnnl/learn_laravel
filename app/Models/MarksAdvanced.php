<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\Pivot;


class MarksAdvanced extends Pivot
{
    protected $table = 'marks';
    public function student() {
        return $this->belongsTo('App\Models\Student','student_id');
    }
    public function subject() {
        return $this->belongsTo('App\Models\Student','subject_id');
    }
    public $timestamps = false;

}