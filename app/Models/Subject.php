<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $table = 'subjects';
    protected $fillable = [
        'name',
    ];
//    protected $hidden = ['pivot'];
    public function marks () {
        return $this->belongsToMany( 'App\Models\Student', 'marks' )->using( 'App\Models\MarksAdvanced');
    }
}
