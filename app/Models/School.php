<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    protected $table = 'schools';
    protected $fillable = [
        'name',
        'address',
    ];
    public $timestamps = false;

    public function students (  ) {
        return $this->hasMany('App\Models\Student','school_id','id');
    }

}
