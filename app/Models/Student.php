<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $table = 'students';

    public function marks () {
        return $this->belongsToMany( 'App\Models\Subject', 'marks' )->using( 'App\Models\MarksAdvanced');
    }

    public function marks_with_info () {
        return $this->marks()->select( 'type','time', 'mark' )->orderBy('type')->orderBy('time');
    }

    public function scopeBasicInfo ( $query ) {
        return $query->select( 'id', 'name' );
    }


}
