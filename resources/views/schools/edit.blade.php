<form action="{{ route("schools.update",['id' => $school->id])}}" method='post'>
    {{ method_field('PUT') }}
    {{ csrf_field() }}
    Name
    <input type="text" name="name" value="{{ $school->name }}">
    <br>
    Address
    <input type="text" name="address" value="{{ $school->address }}">
    <button>Update</button>
</form>