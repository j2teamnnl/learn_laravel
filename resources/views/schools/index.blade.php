@extends('layout.master')

@section('content')
<a href="{{ route("schools.create") }}">
    Create
</a>
<table class="table">
    <caption>
        <form>
            Search
            <input type="search" name="search" value="{{ $search}}">
        </form>
    </caption>
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Address</th>
        <th>Number of students</th>
        <th>Edit</th>
        <th>Delete</th>
    </tr>
    @foreach ($schools as $school)
        <tr>
            <td>
                {{ $school->id }}
            </td>
            <td>
                {{ $school->name }}
            </td>
            <td>
                {{ $school->address }}
            </td>
            <td>
                {{ $school->students_count }}
            </td>
            <td>
                <a href="{{ route("schools.edit",['id' => $school->id]) }}">
                    Edit
                </a>
            </td>
            <td>
                <form action="{{ route("schools.destroy",['id' => $school->id]) }}" method="post">
                    {{ method_field('DELETE') }}
                    {{ csrf_field() }}
                    <button>Delete</button>
                </form>
            </td>
        </tr>
    @endforeach
</table>

{{ $schools->appends(['search' => $search])->links() }}
@endsection