@extends('layout.master')

@section('content')
<form action="{{ route('marks.view') }}" method='post'>
    {{ csrf_field() }}
    Schools
    <select name="school_id">
        @foreach ($schools as $school)
            <option value="{{ $school->id}}"
            @if ($school->id == $school_id)
                selected
            @endif
            >
                {{ $school->name }}
            </option>
        @endforeach
    </select>
    <br>
    Subjects
    <select name="subject_id">
        @foreach ($subjects as $subject)
            <option value="{{ $subject->id}}"
            @if ($subject->id == $subject_id)
            selected
            @endif>
                {{ $subject->name }}
            </option>
        @endforeach
    </select>
    <br>
    <button>Choose</button>
</form>
<form action="{{ route('marks.process') }}" method="post">
    <input type="hidden" name="subject_id" value="{{ $subject_id }}">
    {{ csrf_field() }}
    <table class="table">
        <tr>
            <th rowspan="2">ID</th>
            <th rowspan="2">Name</th>
            @foreach($max_times as $type => $max_time)
                <th colspan="{{ $max_time }}">
                    @if ($type==0)
                        Final
                    @else
                        Skill
                    @endif
                </th>
            @endforeach
        </tr>
        <tr>
            @foreach($max_times as $type => $max_time)
                @for($i = 1; $i <= $max_time; $i++)
                    <th>
                        {{ $i }}
                    </th>
                @endfor
            @endforeach
        </tr>
        @foreach ($students as $student)
            <tr>
                <td>
                    {{ $student->id }}
                </td>
                <td>
                    {{ $student->name }}
                </td>
                @foreach($max_times as $type => $max_time)
                    @for($i = 1; $i <= $max_time; $i++)
                        <td>
                            <input type="number" name="marks[{{ $student->id }}][{{ $type }}][{{ $i }}]" value="{{ $marks[$student->id][$type][$i] ?? '' }}">
                        </td>
                    @endfor
                @endforeach
            </tr>
        @endforeach
    </table>
    <button>Ok</button>
</form>
@endsection
