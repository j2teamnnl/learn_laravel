@extends('layout.master')

@section('content')
<form action="{{ route('marks_advanced.view') }}" method='post'>
    {{ csrf_field() }}
    Schools
    <select name="school_id">
        @foreach ($schools as $school)
            <option value="{{ $school->id}}">
                {{ $school->name }}
            </option>
        @endforeach
    </select>
    <br>
    Subjects
    <select name="subject_id">
        @foreach ($subjects as $subject)
            <option value="{{ $subject->id}}">
                {{ $subject->name }}
            </option>
        @endforeach
    </select>
    <br>
    <button>Choose</button>
</form>
@endsection