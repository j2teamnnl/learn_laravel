@extends('layout.master')

@section('content')
    <form action="{{ route('marks_advanced.view') }}" method='post'>
        {{ csrf_field() }}
        Schools
        <select name="school_id">
            @foreach ($schools as $school)
                <option value="{{ $school->id}}"
                        @if ($school->id == $school_id)
                        selected
                        @endif
                >
                    {{ $school->name }}
                </option>
            @endforeach
        </select>
        <br>
        Subjects
        <select name="subject_id">
            @foreach ($subjects as $subject)
                <option value="{{ $subject->id}}"
                        @if ($subject->id == $subject_id)
                        selected
                        @endif>
                    {{ $subject->name }}
                </option>
            @endforeach
        </select>
        <br>
        <button>Choose</button>
    </form>
    <form action="{{ route('marks.process') }}" method="post">
        <input type="hidden" name="subject_id" value="{{ $subject_id }}">
        {{ csrf_field() }}
        <table class="table">
            <tr>
                <th rowspan="2">ID</th>
                <th rowspan="2">Name</th>
                @foreach($max_times_each_type as $each)
                    <th colspan="{{ $each->max_time+1 }}">
                        @if ($each->type==0)
                            Final
                        @else
                            Skill
                        @endif
                    </th>
                @endforeach
            </tr>
            <tr>
                @foreach($max_times_each_type as $each)
                    @for($time = 1; $time <= $each->max_time+1; $time++)
                        <th>
                            {{ $time }}
                        </th>
                    @endfor
                @endforeach
            </tr>
            @foreach ($student_with_mark as $student)
                @php
                    $student_id = $student->id;
                @endphp
                <tr>
                    <td>
                        {{ $student_id }}
                    </td>
                    <td>
                        {{ $student->name }}
                    </td>
                    @foreach($max_times_each_type as $each)
                        @for($time = 1; $time <= $each->max_time+1; $time++)
                            <td>
                                <input type="number" name="marks[{{ $student_id }}][{{ $each->type }}][{{ $time }}]"
                                       value="{{ $marks[$student_id][$each->type][$time] ?? '' }}">
                            </td>
                        @endfor
                    @endforeach
                </tr>
            @endforeach
        </table>
        <button>Ok</button>
    </form>
@endsection
