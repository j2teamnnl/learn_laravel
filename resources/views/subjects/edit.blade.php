<form action="{{ route("subjects.update",['id' => $subject->id])}}" method='post'>
    {{ method_field('PUT') }}
    {{ csrf_field() }}
    Name
    <input type="text" name="name" value="{{ $subject->name }}">
    <button>Update</button>
</form>