<a href="{{ route("subjects.create") }}">
    Create
</a>
<table>
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Address</th>
        <th>Edit</th>
        <th>Delete</th>
    </tr>
    @foreach ($subjects as $subject)
        <tr>
            <td>
                {{ $subject->id }}
            </td>
            <td>
                {{ $subject->name }}
            </td>
            <td>
                {{ $subject->address }}
            </td>
            <td>
                <a href="{{ route("subjects.edit",['id' => $subject->id]) }}">
                    Edit
                </a>
            </td>
            <td>
                <form action="{{ route("subjects.destroy",['id' => $subject->id]) }}" method="post">
                    {{ method_field('DELETE') }}
                    {{ csrf_field() }}
                    <button>Delete</button>
                </form>
            </td>
        </tr>
    @endforeach
</table>