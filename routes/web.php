<?php

Route::get('view_login','LoginController@view_login')->name('view_login');
Route::post('process_login','LoginController@process_login')->name('process_login');

Route::group(['middleware' => 'CheckLogin'],function(){
    Route::resource('schools','SchoolController');
    Route::resource('subjects','SubjectController');

    Route::get('layout','Controller@master');
    Route::get('logout','LoginController@logout');

    Route::get('marks/choose','MarkController@choose');
    Route::post('marks/view','MarkController@view')->name('marks.view');
    Route::post('marks/process','MarkController@process')->name('marks.process');

    Route::get('marks_advanced/choose','MarkController@choose_advanced');
    Route::post('marks_advanced/view','MarkController@view_advanced')->name('marks_advanced.view');
    Route::post('marks_advanced/process','MarkController@process_advanced')->name('marks_advanced.process');
});
